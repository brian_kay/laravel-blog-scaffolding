<?php

use Illuminate\Support\Facades\Route;

Route::resource('posts', App\Http\Controllers\PostsController::class);

Route::resource('topics', App\Http\Controllers\TopicsController::class);

Route::resource('tags', App\Http\Controllers\TagsController::class);

Route::resource('comments', App\Http\Controllers\CommentsController::class);
