<li class="nav-item {{ Request::is('posts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('posts.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Posts</span>
    </a>
</li>
<li class="nav-item {{ Request::is('topics*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('topics.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Topics</span>
    </a>
</li>
<li class="nav-item {{ Request::is('tags*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('tags.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Tags</span>
    </a>
</li>
<li class="nav-item {{ Request::is('comments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('comments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Comments</span>
    </a>
</li>
