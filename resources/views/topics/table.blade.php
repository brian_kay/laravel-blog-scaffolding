<div class="table-responsive-sm">
    <table class="table table-striped" id="topics-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Thumbnail</th>
                <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($topics as $topic)
            <tr>
                <td>{{ $topic->name }}</td>
                <td>{{ $topic->thumbnail }}</td>
                <td>{{ $topic->description }}</td>
                <td>
                    {!! Form::open(['route' => ['topics.destroy', $topic->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('topics.show', [$topic->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('topics.edit', [$topic->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>