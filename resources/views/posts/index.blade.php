@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Posts</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('layouts.messages')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Posts
                             <a class="pull-right" href="{{ route('posts.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('posts.table')
                              <div class="pull-right mr-3">
                                  <div class="row">
                                      {!! $records->render() !!}
                                  </div>
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

