<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Comment;
use Illuminate\Http\Request;
use Flash;
use Response;

class CommentsController extends Controller
{

    /**
     * Display a listing of the Comment.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $comments = Comment::paginate(10);

        return view('comments.index')
            ->with('comments', $comments);
    }

    /**
     * Show the form for creating a new Comment.
     * @return Response
     */
    public function create()
    {
        return view('comments.create');
    }

    /**
     * Store a newly created Comment in storage.
     * @param CreateCommentRequest $request
     * @return Response
     */
    public function store(CreateCommentRequest $request)
    {
        $input = $request->all();

        $comment = Comment::create($input);

        //Flash::success('Comment saved successfully.');

        return redirect(route('comments.index'));
    }

    /**
     * Display the specified Comment.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);

        if (empty($comment)) {
            //Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        return view('comments.show')->with('comment', $comment);
    }

    /**
     * Show the form for editing the specified Comment.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);

        if (empty($comment)) {
            //Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        return view('comments.edit')->with('comment', $comment);
    }

    /**
     * Update the specified Comment in storage.
     * @param int $id
     * @param UpdateCommentRequest $request
     * @return Response
     */
    public function update($id, UpdateCommentRequest $request)
    {
        $comment = Comment::find($id);

        if (empty($comment)) {
            //Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        $comment = Comment::update($request->all(), $id);

        //Flash::success('Comment updated successfully.');

        return redirect(route('comments.index'));
    }

    /**
     * Remove the specified Comment from storage.
     * @param int $id
     * @throws \Exception
     * @return Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);

        if (empty($comment)) {
            //Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        Comment::delete($id);

        //Flash::success('Comment deleted successfully.');

        return redirect(route('comments.index'));
    }
}
