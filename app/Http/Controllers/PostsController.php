<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Flash;
use Response;

class PostsController extends Controller
{
    /**
     * Display a listing of the Post.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $posts = Post::paginate(10);

        return view('posts.index')
            ->with('posts', $posts);
    }

    /**
     * Show the form for creating a new Post.
     * @return Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created Post in storage.
     * @param CreatePostRequest $request
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();

        $post = Post::create($input);

        //Flash::success('Post saved successfully.');

        return redirect(route('posts.index'));
    }

    /**
     * Display the specified Post.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        if (empty($post)) {
            //Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        if (empty($post)) {
            //Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified Post in storage.
     * @param int $id
     * @param UpdatePostRequest $request
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        $post = Post::find($id);

        if (empty($post)) {
            //Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        $post = Post::update($request->all(), $id);

        //Flash::success('Post updated successfully.');

        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified Post from storage.
     * @param int $id
     * @throws \Exception
     * @return Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if (empty($post)) {
            //Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        Post::delete($id);

        //Flash::success('Post deleted successfully.');

        return redirect(route('posts.index'));
    }
}
