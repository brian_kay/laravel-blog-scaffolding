<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Tag
 * @package App\Models
 * @version December 7, 2021, 7:53 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $posts
 * @property string $name
 * @property string $slug
 */
class Tag extends Model
{
    use SoftDeletes;


    public $table = 'tags';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     * @var array
     */
    public static $rules = [
        'name' => 'string|max:64',
        'slug' => 'string|max:64'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function posts()
    {
        return $this->belongsToMany(\App\Models\Post::class, '', 'id', 'id');
    }
}
