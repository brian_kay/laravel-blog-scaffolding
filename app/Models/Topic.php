<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Topic
 * @package App\Models
 * @version December 7, 2021, 7:37 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $posts
 * @property string $name
 * @property string $thumbnail
 * @property string $description
 */
class Topic extends Model
{
    use SoftDeletes;


    public $table = 'topics';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'thumbnail',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'thumbnail' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     * @var array
     */
    public static $rules = [
        'name' => 'string|max:64',
        'thumbnail' => 'image',
        'description' => 'string|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function posts()
    {
        return $this->hasMany(\App\Models\Post::class, 'topic_id', 'id');
    }
}
