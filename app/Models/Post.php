<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Post
 * @package App\Models
 * @version December 6, 2021, 3:48 pm UTC
 *
 * @property \App\Models\User $id
 * @property string $title
 * @property string $slug
 * @property integer $user_id
 * @property string $thumbnail
 * @property string $image
 * @property string $excerpt
 * @property string $body
 * @property string $status
 */
class Post extends Model
{
    use SoftDeletes;


    public $table = 'posts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'user_id',
        'thumbnail',
        'image',
        'excerpt',
        'body',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'user_id' => 'integer',
        'thumbnail' => 'string',
        'image' => 'string',
        'excerpt' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     * @var array
     */
    public static $rules = [
        'title' => 'string|max:120',
        'slug' => 'string|max:120',
        'thumbnail' => 'image',
        'image' => 'image',
        'status' => 'enum:draft,hidden,published'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function id()
    {
        return $this->belongsTo(\App\Models\User::class, 'id', 'user_id');
    }
}
