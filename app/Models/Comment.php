<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Comment
 * @package App\Models
 * @version December 7, 2021, 8:09 am UTC
 *
 * @property integer $user_id
 * @property integer $post_id
 * @property string $body
 * @property string $status
 */
class Comment extends Model
{
    use SoftDeletes;


    public $table = 'comments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'post_id',
        'body',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'post_id' => 'integer',
        'body' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     * @var array
     */
    public static $rules = [
        'body' => 'string|max:1023',
        'status' => 'enum:approved,pending,spam'
    ];

    
}
