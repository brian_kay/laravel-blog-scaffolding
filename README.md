# Laravel Blog Scaffolding

Basic files to add to a Laravel 7 or 8 project to get started with a blog

## Usage
1. Clone this repo or download it as an archive and extract.
2. Move the contents of the repo into the root directory of your Laravel project
3. Either copy the contents of `routes/blog.php` into `routes/web.php` or add the following code into `app\Providers\RouteServiceProvider.php` inside the `boot()` method:
```php
$this->routes(function () {
    Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/user.php'));
});
```
4. Either add `layout/admin.blade.php` or change the layout being extended in the blade files. Do the same for `layout/app.blade.php`
5. Run migrations with `php artisan migrate`
6. Do your frontend magic. This repo does not have frontend designs. You will have to make your own blog and post pages

## Why not build it as a package?
That may be done in future, but for now, I just wanted to share something that people can use and customise without having to deal with unwanted code hidden behind the _vendor_ veil 